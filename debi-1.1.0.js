
/**
 * Library - debi
 *
 * Asynchronous API for client-side storage
 *   - getItem(key)
 *   - setItem(key, value)
 *   - removeItem(key)
 *
 * Features:
 * - ability to store objects as well as strings (just like localforage)
 *
 * Dependencies: ES-6 Promises
 */
(function(global) {
	'use strict';

	var DRIVER_TYPES = {
		'indexedDB': 'IndexedDB',
		'localStorage': 'Web Storage',
		'sessionStorage': 'Web Storage',
		'window.name': 'memory',
		'window.temp': 'memory'
	};
	var driverPreference = [
		'localStorage',
		'indexedDB',
		'sessionStorage',
		'window.name',
		'window.temp'
	];
	var driverSupport = [];
	var driverInUse;
	var driverName;

	var WAIT_READY_PERIOD = 200; // if driver is not ready when called, how long to wait before retrying
	var KEY_PREFIX = 'debi-';
	var IDB_STORE = 'debiStore'; // we only need 1 object store for IndexedDB
	var IDB_VERSION = 2; // increase when we change IndexedDB schema

/************
 *  Global  *
 ************/
	/**
	 * Gets or sets the name of the driver in use.
	 * - Note: if no param, simply returns the current driver
	 * - Note: when debi has init, driver should not be null
	 * @param {string=} x - name of driver to set (optional)
	 * @return {string | null} name of driver in use, null if no driver in use
	 */
	function driver(x) {
		if (x) {
			return setDriver(x);
		}
		return driverName;
	} // driver()

	/**
	 * Sets the preferred driver or drivers.
	 * - Note: does not guarantee use of the driver; actual use depends on availability.
	 *         driver support is unstable during debi init, recommend to call only after debi init
	 *         check mode()
	 * @param {string | Array}
	 * @return {string | null} name of driver in use
	 */
	function setDriver(arg) {
		// console.debug('setDriver() - setting driver preference: ', arg);
		// Process args - set driver preferences
		var i, len; // loop vars
		if (typeof arg === 'string') {
			if (!driverSupport.includes(arg)) {
				throw new RangeError('setDriver() - does not support this driver: ' + arg);
			}

			// Make user-set driver as the first preferred driver
			var index = driverPreference.indexOf(arg);
			if (index === -1) {
				throw new ReferenceError('setDriver() - could not find driver: ' + arg);
			}
			driverPreference.splice(index, 1);
			driverPreference.unshift(arg);
		} // arg is string
		else if (arg instanceof Array) {
			for (i = 0, len = arg.length; i < len; i++) {
				if (!driverSupport.includes(arg[i])) {
					throw new RangeError('setDriver() - does not support this driver: ' + arg[i] + ' (argument index: ' + i + ')');
				}
			}
			driverPreference = arg;
		} // arg is array
		else {
			throw new TypeError('setDriver() - expects string or array arg');
		}

		// Find first supported driver
		for (i = 0, len = driverPreference.length; i < len; i++) {
			if (driverSupport.includes(driverPreference[i])) {
				driverName = driverPreference[i];
				break;
			}
		}
		if (driverName) {
			var driver_type = DRIVER_TYPES[driverName];
			// console.debug('setDriver() - Selected ' + driverName + ', driver type: ' + driver_type);

			if (driver_type === 'Web Storage') {
				driverInUse = makeDriverForWebStorage(driverName);
			}
			else if (driver_type === 'IndexedDB') {
				driverInUse = makeDriverForIndexedDB(driverName);
			}
			else if (driver_type === 'memory') {
				driverInUse = makeDriverForMemory(driverName);
			}
			else {
				throw new Error('setDriver() - illegal driver type: ' + driver_type);
			}

			// console.debug(driverInUse);
		}

		return driverName;
	} // setDriver()

	/**
	 * Checks the storage engines supported by this browser
	 * @param {string=} x - name of driver to check (optional)
	 * @return {boolean | Array} if no argument passed in, returns an Array of supported engine names (strings)
	 */
	function supports(x) {
		if (typeof x === 'undefined') {
			return driverSupport;
		}

		return driverSupport.includes(x);
	} // supports()

	/**
	 * Returns the persistence mode ["session", "local", "none"]
	 * - Note: when debi has init, mode should not be "none"
	 * @return {string}
	 */
	function mode() {
		if (driverName === 'window.temp') {
			return 'volatile';
		}
		if (driverName === 'sessionStorage' || driverName === 'window.name') {
			return 'session';
		}
		if (driverName === 'localStorage' || driverName === 'indexedDB') {
			return 'local';
		}

		return 'none';
	} // mode()

/*****************
 *  Data Engine  *
 *****************/

	/**
	 * @private
	 * Returns a driver based on a Storage engine.
	 * @param {string} engine_name - localStorage or sessionStorage
	 * @return {Object} with API of a driver
	 */
	function makeDriverForWebStorage(engine_name) {
		var engine = global[engine_name];
		if (!(engine instanceof Storage)) {
			return null;
		}

		function webStorageGetItem(key) {
			var result = deserialize(engine.getItem(KEY_PREFIX + key));

			return Promise.resolve(result);
		} // webStorageGetItem()

		function webStorageSetItem(key, value) {
			engine.setItem(KEY_PREFIX + key, serialize(value));

			return Promise.resolve(value);
		} // webStorageSetItem()

		function webStorageRemoveItem(key) {
			engine.removeItem(KEY_PREFIX + key);

			return Promise.resolve();
		} // webStorageRemoveItem()

		function webStorageHasItem(key) {
			return webStorageKeys().then(function(ks) {
				return ks.includes(key);
			});
		} // webStorageHasItem()

		function webStorageClear() {
			return webStorageKeys().then(function(ks) {
				ks.forEach(function(key) {
					engine.removeItem(KEY_PREFIX + key);
				});

				return Promise.resolve();
			});
		} // webStorageClear()

		function webStorageLength() {
			return webStorageKeys().then(function(ks) {
				return ks.length;
			});
		} // webStorageLength()

		function webStorageKeys() {
			var ks = [];

			var len = engine.length;
			for (var i = 0; i < len; i++) {
				var key = engine.key(i);

				if (!key.startsWith(KEY_PREFIX)) {
					continue;
				}

				key = key.substring(KEY_PREFIX.length);
				ks.push(key);
			}

			return Promise.resolve(ks);
		} // webStorageKeys()

		/* Quite useless
		function key(i) {
			var result;
			try {
				result = engine.key(i);

				if (result.startsWith(KEY_PREFIX)) {
					result = result.substring(KEY_PREFIX.length);
				}
				else {
					result = null;
				}
			} catch (error) {
				result = null;
			}

			return Promise.resolve(result);
		} //key()
		*/

		/* Incompatible with IE
		function iterate(iterator) {
			var len = engine.length;
			for (var i = 0; i < len; i++) {
				var key = engine.key(i);
				if (!key.startsWith(KEY_PREFIX)) {
					continue;
				}
				var value = engine.getItem(key);
				if (value) {
					value = deserialize(value);
				}

				value = iterator(value, key.substring(KEY_PREFIX.length), i);

				// Check for early exit
				if (typeof value !== 'undefined') {
					return Promise.resolve(value);
				}
			} // for each value in Storage

			// Resolve with undefined if iterator function doesn't return
			return Promise.resolve();
		} //iterate()
		*/

		return {
			'getItem': webStorageGetItem,
			'setItem': webStorageSetItem,
			'removeItem': webStorageRemoveItem,
			'hasItem': webStorageHasItem,
			'clear': webStorageClear,
			'length': webStorageLength,
			// 'iterate': iterate,
			// 'key': key,
			'keys': webStorageKeys
		};
	} // makeDriverForWebStorage()

	/**
	 * @private
	 * Returns a driver built based on storing data in-memory (under a global variable).
	 * @param {string} engine_name - e.g. "window.name", "window.temp"
	 * @return {Object} with API of a driver
	 */
	function makeDriverForMemory(engine_name) {
		var data;

		// Init
		if (engine_name.startsWith('window.')) {
			engine_name = engine_name.substring(7); // eslint-disable-line no-magic-numbers, no-param-reassign
		}
		try {
			data = JSON.parse(global[engine_name]);
		}
		catch (e) {
			// console.debug('Error parsing data from source. Possibly no previous data.');
			data = {};
		}

		function writeToMemory() {
			global[engine_name] = JSON.stringify(data);
		} // writeToMemory()

		function memoryGetItem(key) {
			if (typeof data[key] === 'undefined') {
				return Promise.resolve(null);
			}

			return Promise.resolve(data[key]);
		} // memoryGetItem()

		function memorySetItem(key, value) {
			// Add to data
			data[key] = value;

			writeToMemory();
			return Promise.resolve(value);
		} // memorySetItem()

		function memoryRemoveItem(key) {
			if (typeof data[key] !== 'undefined') {
				delete data[key];
				writeToMemory();
			}

			return Promise.resolve();
		} // memoryRemoveItem()

		function memoryHasItem(key) {
			var has_key = data.hasOwnProperty(key);

			return Promise.resolve(has_key);
		} // memoryHasItem()

		function memoryClear() {
			data = {};
			writeToMemory();

			return Promise.resolve();
		} // memoryClear()

		function memoryLength() {
			var data_keys = Object.keys(data);

			return Promise.resolve(data_keys.length);
		} // memoryLength()

		function memoryKeys() {
			var data_keys = Object.keys(data);

			return Promise.resolve(data_keys);
		} // memoryKeys()

		/* Quite useless
		function key(i) {
			var keys = Object.keys(data);
			if (i < keys.length) {
				return Promise.resolve(keys[i]);
			}
			else {
				return Promise.resolve(null);
			}
		} // key()
		*/

		/* Incompatible with IE
		function iterate() {
			for (var key of data) {
				var value = data[key];

				var iterator = iterator(value, key.substring(KEY_PREFIX.length), i);

				// Check for early exit
				if (typeof iterator !== 'undefined') {
					return Promise.resolve(iterator);
				}
			} // for each value in data

			// Resolve with undefined if iterator function doesn't return
			return Promise.resolve(undefined);
		} // iterate()
		*/

		return {
			'getItem': memoryGetItem,
			'setItem': memorySetItem,
			'removeItem': memoryRemoveItem,
			'hasItem': memoryHasItem,
			'clear': memoryClear,
			'length': memoryLength,
			// 'iterate': iterate,
			// 'key': key,
			'keys': memoryKeys
		};
	} // makeDriverForMemory()

	/**
	 * @private
	 * Returns a driver built based on storing data in-memory (under a global variable).
	 * @param {string} engine_name - probably indexedDB
	 * @return {Object} with API of a driver
	 */
	function makeDriverForIndexedDB(engine_name) {
		var db;

		var open_db_request = global[engine_name].open(KEY_PREFIX + 'db', IDB_VERSION);
		open_db_request.onupgradeneeded = function(evt) {
			// console.debug('makeDriverForIndexedDB() - we are in onupgradeneeded');
			db = evt['target']['result'];
			db.createObjectStore(IDB_STORE, {'keyPath': 'key'});
		};
		open_db_request.onerror = function(evt) {
			// console.debug('makeDriverForIndexedDB() - NOT Supported: indexedDB. (request.onerror)');

			console.error(evt);
			throw new Error('makeDriverForIndexedDB() - unable to open: ' + KEY_PREFIX + 'db');
		};
		open_db_request.onsuccess = function(evt) {
			// console.debug('makeDriverForIndexedDB() - we are in request.onsuccess');

			db = evt['target']['result'];
		};

		function idbGetItem(key) {
			// Ensure DB is opened
			if (!db) {
				return wait(WAIT_READY_PERIOD).then(function() {
					return idbGetItem(key);
				});
			}

			return new Promise(function(resolve, reject) {
				var request = db.transaction([IDB_STORE], 'readonly')
				              .objectStore(IDB_STORE)
				              .get(key);

				request.onerror = function(evt) {
					console.error(evt);
					reject(new Error('Unable to get() ' + key + ' [idbGetItem]'));
				};

				request.onsuccess = function() {
					if (typeof request['result'] === 'undefined') {
						resolve(null); // in accordance with Web Storage spec: return null if no key found
					}
					else {
						resolve(request['result']['value']);
					}
				};
			});
		} // idbGetItem()

		function idbSetItem(key, value) {
			// Ensure DB is opened
			if (!db) {
				return wait(WAIT_READY_PERIOD).then(function() {
					return idbSetItem(key, value);
				});
			}

			var data = {
				'key': key,
				'value': value
			};

			return new Promise(function(resolve, reject) {
				var request = db.transaction([IDB_STORE], 'readwrite')
				              .objectStore(IDB_STORE)
				              .put(data);

				request.onerror = function(evt) {
					console.error(evt);
					reject(new Error('Unable to put() ' + key + ' [idbSetItem]'));
				};

				request.onsuccess = function() {
					resolve(value);
				};
			});
		} // idbSetItem()

		function idbRemoveItem(key) {
			// Ensure DB is opened
			if (!db) {
				return wait(WAIT_READY_PERIOD).then(function() {
					return idbRemoveItem(key);
				});
			}

			return new Promise(function(resolve, reject) {
				var request = db.transaction([IDB_STORE], 'readwrite')
				              .objectStore(IDB_STORE)
				              .delete(key);

				request.onerror = function(evt) {
					console.error(evt);
					reject(new Error('Unable to delete() ' + key + ' [idbRemoveItem]'));
				};

				request.onsuccess = function() {
					resolve();
				};
			});
		} // idbRemoveItem()

		function idbHasItem(key) {
			// Ensure DB is opened
			if (!db) {
				return wait(WAIT_READY_PERIOD).then(function() {
					return idbHasItem(key);
				});
			}

			return new Promise(function(resolve, reject) {
				var request = db.transaction([IDB_STORE], 'readonly')
				             .objectStore(IDB_STORE)
				             .get(key);

				request.onerror = function(evt) {
					console.error(evt);
					reject(new Error('Unable to get() ' + key + ' [idbGetItem]'));
				};

				request.onsuccess = function() {
					resolve(typeof request['result'] !== 'undefined');
				};
			});
		} // idbHasItem()

		function idbClear() {
			// Ensure DB is opened
			if (!db) {
				return wait(WAIT_READY_PERIOD).then(function() {
					return idbClear();
				});
			}

			return new Promise(function(resolve, reject) {
				var request = db.transaction([IDB_STORE], 'readwrite')
				             .objectStore(IDB_STORE)
				             .clear();

				request.onerror = function(evt) {
					console.error(evt);
					reject(new Error('Unable to clear() [idbClear]'));
				};

				request.onsuccess = function() {
					resolve();
				};
			});
		} // idbClear()

		function idbLength() {
			// Ensure DB is opened
			if (!db) {
				return wait(WAIT_READY_PERIOD).then(function() {
					return idbLength();
				});
			}

			return new Promise(function(resolve, reject) {
				var request = db.transaction([IDB_STORE], 'readonly')
				             .objectStore(IDB_STORE)
				             .count();

				request.onerror = function(evt) {
					console.error(evt);
					reject(new Error('Unable to execute count() [idbLength]'));
				};

				request.onsuccess = function() {
					resolve(request['result']);
				};
			});
		} // idbLength()

		function idbKeys() {
			// Ensure DB is opened
			if (!db) {
				return wait(WAIT_READY_PERIOD).then(function() {
					return idbKeys();
				});
			}

			return new Promise(function(resolve, reject) {
				var request = db.transaction([IDB_STORE], 'readonly')
				              .objectStore(IDB_STORE)
				              .getAllKeys();

				request.onerror = function(evt) {
					console.error(evt);
					reject(new Error('Unable to execute getAllKeys() [idbKeys]'));
				};

				request.onsuccess = function() {
					resolve(request['result']);
				};
			});
		} // idbKeys()

		return {
			'getItem': idbGetItem,
			'setItem': idbSetItem,
			'removeItem': idbRemoveItem,
			'hasItem': idbHasItem,
			'clear': idbClear,
			'length': idbLength,
			// 'iterate': iterate,
			// 'key': key,
			'keys': idbKeys
		};
	} // makeDriverForIndexedDB()

/*************
 *  Helpers  *
 *************/
	/**
	 * @private
	 */
	function deserialize(value) {
		return value && JSON.parse(value); // because of short-circuit evaluation, null will immediately return rather than get JSON.parse
	} // deserialize()
	function serialize(value) {
		if (typeof value === 'undefined') {
			return 'null';
		}
		return JSON.stringify(value);
	} // serialize()

	/**
	 * @private
	 * @param {number} period
	 * @return {Promise}
	 */
	function wait(period) {
		// console.debug('debi.wait() - Will wait ' + period + 'ms for driver to be ready.');
		return new Promise(function(resolve) {
			setTimeout(resolve, period);
		});
	} // wait()

/*************
 * Detection *
 *************/
	/**
	 * @private
	 * Checks browser support for localStorage or sessionStorage
	 * @param {string} engine_name - either "localStorage" or "sessionStorage"
	 * @return {Promise} resolves with {string} engine supported, i.e. "localStorage" or "sessionStorage"
	 */
	function detectWebStorage(engine_name) {
		return new Promise(function(resolve) {
			try {
				var engine = global[engine_name];


				// Set, get and remove
				var test, result;
				test = KEY_PREFIX + 'test';
				engine.setItem(test, test);
				result = engine.getItem(test);
				engine.removeItem(test);

				// if we get correct result, we can assume engine support
				if (result === test) {
					// console.debug('detectWebStorage() - Supported: ' + engine_name);
					resolve(engine_name);
				}

				resolve();
			} // try
			catch (exception) {
				// console.debug('detectWebStorage() - NOT Supported: ' + engine_name)
				// console.debug(exception);

				resolve();
			}
		});
	} // detectWebStorage()

	/**
	 * @private
	 * Checks browser support for IndexedDB
	 * @return {Promise} resolves with {string} engine supported, i.e. "indexedDB" or "" (no support)
	 */
	function detectIndexedDB() {
		return new Promise(function(resolve) {
			var db;

			function testTransaction() {
				var transaction = db.transaction([IDB_STORE], 'readwrite');
				transaction.onerror = function() {
					db.close();

					throw new Error('detectIndexedDB() - transaction.onerror');
				};
				transaction.oncomplete = function() {
					db.close();

					resolve('indexedDB');
				};
			}

			try {
				if (!global['indexedDB']) {
					throw new Error('No indexedDB object');
				}

				var open_db_request = global['indexedDB'].open(KEY_PREFIX + 'db', IDB_VERSION);
				open_db_request.onupgradeneeded = function(evt) {
					// console.debug('detectIndexedDB() - we are in onupgradeneeded - should only be here on first db creation');
					db = evt['target']['result'];
					db.deleteObjectStore(IDB_STORE, {'keyPath': 'key'});
					db.createObjectStore(IDB_STORE, {'keyPath': 'key'});
					evt['target']['transaction'].oncomplete = testTransaction;
				};
				open_db_request.onerror = function(evt) {
					// console.debug('detectIndexedDB() - NOT Supported: indexedDB. (open_db_request.onerror)');
					console.error(evt);

					resolve();
					return false;
				};
				open_db_request.onsuccess = function(evt) {
					// console.debug('detectIndexedDB() - we are in open_db_request.onsuccess');
					db = evt['target']['result'];
					testTransaction();
					// testTransaction();
				};
			}
			catch (e) {
				console.error(e);
				// console.debug('detectIndexedDB() - NOT Supported: IndexedDB');

				resolve();
			}
		});
	} // detectIndexedDB()

	/***
	 * @private
	 * Detects different engine support and sets the driver according to preference
	 */
	function init() {
		// memory-based engines are definitely supported
		driverSupport.push('window.name');
		driverSupport.push('window.temp');

		// Detect support
		return Promise.all([detectWebStorage('localStorage'), detectWebStorage('sessionStorage'), detectIndexedDB()])
		.then(function(detected_supports) {
			detected_supports.forEach(function(d) {
				if (d) {
					driverSupport.push(d);
				}
			});

			// Remove unsupported engines from driver preferences
			driverPreference = driverPreference.filter(function(el) {
				return driverSupport.includes(el);
			});
			setDriver(driverPreference);
		})
		.catch(function(e) {
			console.error(e);
		});
	} // init()

/*********************
 *  Data Operations  *
 *********************/
	/**
	 * Note: even if NaN / undefined is saved, null will be returned by getItem()
	 *       note: value === null if not found
	 * @param {string} key
	 * @return {Promise=} resolves with value gotten
	 */
	function getItem(key) {
		if (typeof key !== 'string') {
			console.warn(key + ' (non-string) used as a key.');
			key = String(key);  // eslint-disable-line no-param-reassign
		}

		if (!driverInUse) {
			return wait(WAIT_READY_PERIOD).then(function() {
				return getItem(key);
			});
		}

		return driverInUse['getItem'](key);
	} // getItem()

	/**
	 * @param {string} key
	 *        {?} value
	 * @return {Promise} resolves with value stored
	 */
	function setItem(key, value) {
		if (typeof key !== 'string') {
			console.warn(key + ' (non-string) used as a key.');
			key = String(key); // eslint-disable-line no-param-reassign
		}

		if (!driverInUse) {
			return wait(WAIT_READY_PERIOD).then(function() {
				return setItem(key, value);
			});
		}

		return driverInUse['setItem'](key, value);
	} // setItem()

	/**
	 * @param {string} key
	 * @return {Promise} resolves with undefined
	 */
	function removeItem(key) {
		if (typeof key !== 'string') {
			console.warn(key + ' (non-string) used as a key.');
			key = String(key); // eslint-disable-line no-param-reassign
		}

		if (!driverInUse) {
			return wait(WAIT_READY_PERIOD).then(function() {
				return removeItem(key);
			});
		}

		return driverInUse['removeItem'](key);
	} // removeItem()

	/**
	 * @param {string} key
	 * @return {Promise} resolves with boolean
	 */
	function hasItem(key) {
		if (typeof key !== 'string') {
			console.warn(key + ' (non-string) used as a key.');
			key = String(key); // eslint-disable-line no-param-reassign
		}

		if (!driverInUse) {
			return wait(WAIT_READY_PERIOD).then(function() {
				return hasItem(key);
			});
		}

		return driverInUse['hasItem'](key);
	} // hasItem()

	/**
	 * Deletes everything from sessionStorage
	 * @return {Promise} resolves with undefined
	 */
	function clear() {
		if (!driverInUse) {
			return wait(WAIT_READY_PERIOD).then(function() {
				return clear();
			});
		}

		return driverInUse['clear']();
	} // clear()

	/**
	 * @return {Promise} resolves with number
	 */
	function length() { // eslint-disable-line no-shadow
		if (!driverInUse) {
			return wait(WAIT_READY_PERIOD).then(function() {
				return length();
			});
		}

		return driverInUse['length']();
	} // length()

	/**
	 * Removed as this is quite useless
	 * Returns the name of the nth key in the list.
	 * Note: the order of keys is user-agent defined. Adding/Removing items may change the order of keys,
	 *       but changing the value of existing keys must not.
	 *       This weird method is inherited from the WebStorage API.
	 * @param {number} i - expects an unsigned integer
	 * @return {Promise} resolves with string or null
	 */
	/*
	function key(i) {
		if (typeof i !== 'number') {
			throw new TypeError('key() - expects an unsigned integer argument. Got: ' + typeof i);
		}
		if (i % 1 !== 0 || i < 0) {
			throw new RangeError('key() - expects an unsigned integer argument. Got: ' + i);
		}

		if (!driverInUse) {
			return wait(WAIT_READY_PERIOD).then(function() {
				return key(i);
			});
		}

		return driverInUse['key'](i);
	} // key()
	*/

	/**
	 * Incompatible with IE
	 * Iterate over all value/key pairs in datastore.
	 * - Supports early exit on return (non-undefined) in the iterator function.
	 * @param {function} iterator function with three params: (value, key, iterationNumber)
	 * @return {Promise} resolves with iterator function's return value (undefined if iterator function doesn't return)
	 */
	/*
	function iterate(iterator) {
		if (typeof iterator !== 'function') {
			throw new TypeError('iterate() - expects an iterator function. Got: ' + typeof iterator);
		}

		if (!driverInUse) {
			return wait(WAIT_READY_PERIOD).then(function() {
				return iterate(iterator);
			});
		}

		return driverInUse['iterator']();
	} // iterate()
	*/

	/**
	 * Get the list of all keys in the datastore.
	 * @return {Promise} resolves with array of strings
	 */
	function keys() {
		if (!driverInUse) {
			return wait(WAIT_READY_PERIOD).then(function() {
				return keys();
			});
		}

		return driverInUse['keys']();
	} // keys()

	// Export
	init();
	global['debi'] = {
		// Global / config
		'driver': driver,
		'setDriver': setDriver,
		'supports': supports,
		'mode': mode,

		// Data operations (async)
		'getItem': getItem,
		'get': getItem,            // alias
		'setItem': setItem,
		'set': setItem,            // alias
		'removeItem': removeItem,
		'remove': removeItem,      // alias
		'hasItem': hasItem,
		'has': hasItem,            // alias
		'clear': clear,
		'length': length,
		'keys': keys
		// ,'key': key
		// ,'iterate': iterate
	};
}(this));
