/**
 * Library - sessionforage v1.0.0
 * 
 * Asynchronous API for client-side storage
 *   - getItem(key)
 *   - setItem(key, value)
 *   - removeItem(key)
 *
 * Features: 
 * - ability to store objects as well as strings (just like localforage)
 * - can use either callback or Promises (but not both).
 *
 * Dependencies: sessionStorage, ES-6 Promises (if using Promises)
 */
(function(global, sessionStorage) {
	'use strict';
	
	/**
	 * @param: {string} key
	 *         {function} callback (optional)
	 * @return: {Promise} resolves with value gotten (note: value === null if not found)
	 */
	function getItem(key, callback) {
		if (typeof key !== 'string') {	
			console.warn(key + ' (non-string) used as a key.');
			key = String(key);
		}
		
		function doWork(resolve) {
			var result = deserialize(sessionStorage.getItem(key));
			resolve(result);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //getItem()
	
	/**
	 * @param: {string} key
	 *         {string | number | anything serializable} value
	 *         {function} callback (optional)
	 * @return: {Promise} resolves with value stored
	 */
	function setItem(key, value, callback) {
		if (typeof key !== 'string') {	
			console.warn(key + ' (non-string) used as a key.');
			key = String(key);
		}
		
		function doWork(resolve) {
			sessionStorage.setItem(key, serialize(value));
			resolve(value);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //setItem()
	
	/**
	 * @param: {string} key
	 *         {function} callback (optional)
	 * @return: {Promise} resolves with undefined
	 */
	function removeItem(key, callback) {
		if (typeof key !== 'string') {	
			console.warn(key + ' (non-string) used as a key.');
			key = String(key);
		}
		
		function doWork(resolve) {
			sessionStorage.removeItem(key);
			resolve();
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //removeItem()
	
	/**
	 * Deletes everything from sessionStorage
	 * @param: {function} callback (optional)
	 * @return: {Promise} resolves with undefined
	 */
	function clear(callback) {
		function doWork(resolve) {
			sessionStorage.clear();
			resolve();
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //clear()
	
	/**
	 * @param: {function} callback (optional)
	 * @return: {Promise} resolves with number
	 */
	function length(callback) {
		function doWork(resolve) {
			resolve(sessionStorage.length);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //length()
	
	/**
	 * Returns the name of the nth key in the list.
	 * Note: the order of keys is user-agent defined. Adding/Removing items may change the order of keys,
	 *       but changing the value of existing keys must not.
	 *       This weird method is inherited from the WebStorage API.
	 * @param:  {number} n
	 *          {function} callback (optional)
	 * @return: {Promise} resolves with string or null
	 */
	function key(n, callback) {
		function doWork(resolve) {
			var result;
			try {
				result = sessionStorage.key(n);
			} catch (error) {
				result = null;
			}
			
			resolve(result);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //key()
	
	/**
	 * Get the list of all keys in the datastore.
	 * @param: {function} callback (optional)
	 * @return: {Promise} resolves with array of strings
	 */
	function keys(callback) {
		function doWork(resolve) {
			var length = sessionStorage.length;
			var keys = [];
			for (var i = 0; i < length; i++) {
				keys.push(sessionStorage.key(i));
			}
			resolve(keys);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //keys()
	
	/**
	 * Iterate over all value/key pairs in datastore.
	 * - Supports early exit on return (non-undefined) in the iterator function.
	 * @param: {function} iterator function with three params: (value, key, iterationNumber)
	 *         {function} callback (optional)
	 * @return: {Promise} resolves with iterator function's return value (undefined if iterator function doesn't return)
	 */
	function iterate(iterator, callback) {
		function doWork(resolve) {
			var length = sessionStorage.length;
			for (var i = 0; i < length; i++) {
				var key = sessionStorage.key(i);
				var value = sessionStorage.getItem(key);
				if (value) {
					value = deserialize(value);
				}
				value = iterator(value, key, i);
				
				// Check for early exit
				if (typeof value !== 'undefined') {
					resolve(value);
				}
			} // for each value in sessionStorage
			
			// Resolve with undefined if iterator function doesn't return
			resolve(undefined);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //iterate()
	
	/**
	 * @private
	 */
	function deserialize(value) {
		return value && JSON.parse(value); //because of short-circuit evaluation, null will immediately return rather than get JSON.parse
	}
	
	/**
	 * @private
	 */
	function serialize(value) {
		return JSON.stringify(value);
	}
	
	global.sessionforage = {
		getItem: getItem,
		setItem: setItem,
		removeItem: removeItem,
		clear: clear,
		length: length,
		key: key,
		keys: keys,
		iterate: iterate
	};
})(this || {}, this.sessionStorage);
