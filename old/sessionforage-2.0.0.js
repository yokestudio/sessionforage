/**
 * Library - sessionforage
 * 
 * Asynchronous API for client-side storage
 *   - getItem(key)
 *   - setItem(key, value)
 *   - removeItem(key)
 *
 * Features: 
 * - ability to store objects as well as strings (just like localforage)
 * - can use either callback or Promises (but not both).
 *
 * Dependencies: ES-6 Promises (if using Promises)
 */
(function(global) {
	'use strict';
	
	var drivers = {
		'sessionStorage': makeSessionStorageDriver()
	};
	var driverSupport = {
		'sessionStorage': testSupport(drivers['sessionStorage'])
	};
	var preferredDriverOrder = [
		'sessionStorage'
	];
	
	var driverInUse = selectDriverToUse();
	//console.log('[DEBUG] default selected: ' + driver());
	
	/**
	 * Gets the name of the driver in use.
	 * - Note: empty string if no driver is in use
	 * @return {string}
	 */
	function driver() {
		for (var prop in drivers) {
			if (!drivers.hasOwnProperty(prop)) {
				continue;
			}
			
			if (drivers[prop] === driverInUse) {
				return prop;
			}
		}

		return '';
	} // driver()
	
	/**
	 * Sets the preferred driver or drivers.
	 * - Note: does not guarantee use of the driver; actual use depends on availability.
	 * @param {string | Array}
	 */
	function setDriver(arg) {
		if (typeof arg === 'string') {
			if (!driverSupport.hasOwnProperty(arg)) {
				throw new RangeError('sessionforage setDriver() does not support this driver: ' + arg);
			}
			
			// Put user-set driver as the first preferred driver
			var index = preferredDriverOrder.indexOf(arg);
			if (index === -1) {
				throw new ReferenceError('sessionforage setDriver() could not find driver: ' + arg);
			}
			preferredDriverOrder.splice(index, 1);
			preferredDriverOrder.unshift(arg);
		}
		else if (arg instanceof Array) {
			for (var i=0, len=arg.length; i<len; i++) {
				if (!driverSupport.hasOwnProperty(arg[i])) {
					throw new RangeError('sessionforage setDriver() does not support this driver: ' + arg[i] + ' (argument index: ' + i + ')');
				}
			}
			preferredDriverOrder = arg;
		}
		else {
			throw new TypeError('sessionforage setDriver() expects string or array arg');
		}
		
		driverInUse = selectDriverToUse();
	} //setDriver()
	
	/**
	 * Note: even if NaN / undefined is saved, null will be returned by getItem()
	 *       note: value === null if not found
	 * @param {string} key
	 *        {function=} callback (optional)
	 * @return {Promise=} resolves with value gotten
	 */
	function getItem(key, callback) {
		if (typeof key !== 'string') {	
			console.warn(key + ' (non-string) used as a key.');
			key = String(key);
		}
		
		function doWork(resolve) {
			var result = deserialize(driverInUse.getItem(key));
			resolve(result);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //getItem()
	
	/**
	 * @param {string} key
	 *        {string | number | anything serializable} value
	 *        {function=} callback (optional)
	 * @return {Promise} resolves with value stored
	 */
	function setItem(key, value, callback) {
		if (typeof key !== 'string') {	
			console.warn(key + ' (non-string) used as a key.');
			key = String(key);
		}
		
		function doWork(resolve) {
			driverInUse.setItem(key, serialize(value));
			resolve(value);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //setItem()
	
	/**
	 * @param {string} key
	 *        {function=} callback (optional)
	 * @return {Promise} resolves with undefined
	 */
	function removeItem(key, callback) {
		if (typeof key !== 'string') {	
			console.warn(key + ' (non-string) used as a key.');
			key = String(key);
		}
		
		function doWork(resolve) {
			driverInUse.removeItem(key);
			resolve();
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //removeItem()
	
	/**
	 * Deletes everything from sessionStorage
	 * @param {function=} callback (optional)
	 * @return {Promise} resolves with undefined
	 */
	function clear(callback) {
		function doWork(resolve) {
			driverInUse.clear();
			resolve();
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //clear()
	
	/**
	 * @param {function=} callback (optional)
	 * @return {Promise} resolves with number
	 */
	function length(callback) {
		function doWork(resolve) {
			resolve(driverInUse.length);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //length()
	
	/**
	 * Returns the name of the nth key in the list.
	 * Note: the order of keys is user-agent defined. Adding/Removing items may change the order of keys,
	 *       but changing the value of existing keys must not.
	 *       This weird method is inherited from the WebStorage API.
	 * @param {number} n
	 *        {function=} callback (optional)
	 * @return {Promise} resolves with string or null
	 */
	function key(n, callback) {
		function doWork(resolve) {
			var result;
			try {
				result = driverInUse.key(n);
			} catch (error) {
				result = null;
			}
			
			resolve(result);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //key()
	
	/**
	 * Get the list of all keys in the datastore.
	 * @param {function} callback (optional)
	 * @return {Promise} resolves with array of strings
	 */
	function keys(callback) {
		function doWork(resolve) {
			var length = driverInUse.length;
			var keys = [];
			for (var i = 0; i < length; i++) {
				keys.push(driverInUse.key(i));
			}
			resolve(keys);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //keys()
	
	/**
	 * Iterate over all value/key pairs in datastore.
	 * - Supports early exit on return (non-undefined) in the iterator function.
	 * @param {function} iterator function with three params: (value, key, iterationNumber)
	 *        {function} callback (optional)
	 * @return {Promise} resolves with iterator function's return value (undefined if iterator function doesn't return)
	 */
	function iterate(iterator, callback) {
		function doWork(resolve) {
			var length = driverInUse.length;
			for (var i = 0; i < length; i++) {
				var key = driverInUse.key(i);
				var value = driverInUse.getItem(key);
				if (value) {
					value = deserialize(value);
				}
				value = iterator(value, key, i);
				
				// Check for early exit
				if (typeof value !== 'undefined') {
					resolve(value);
				}
			} // for each value in sessionStorage
			
			// Resolve with undefined if iterator function doesn't return
			resolve(undefined);
		}
		
		// Return 
		if (typeof callback === 'function') {
			doWork(callback);
		}
		else {
			return new Promise(doWork);
		}
	} //iterate()
	
	/**
	 * @private
	 */
	function deserialize(value) {
		return value && JSON.parse(value); //because of short-circuit evaluation, null will immediately return rather than get JSON.parse
	}
	function serialize(value) {
		if (typeof value === 'undefined') {
			return 'null';
		}
		return JSON.stringify(value);
	}
	
	/**
	 * @private
	 * @return {boolean}
	 */
	function testSupport(driver){
		try {
			var test, result;
			
			// Set, get and remove
			test = 'sessionforageTestString';
			driver.setItem(test, test);
			result = driver.getItem(test);
			driver.removeItem(test);
			
			// if we get correct result, we can assume driver support
			if (result === test) {
				return true;
			}
		} // try
		catch (exception) {
			return false;
		}
	} //testSupport()
	
	/**
	 * @private
	 * @return {Object} - the driver to use, based on preference setting 
	 */
	function selectDriverToUse() {
		var selected = '';
		for (var i=0, len=preferredDriverOrder.length; i<len; i++) {
			if (driverSupport[preferredDriverOrder[i]]) {
				selected = preferredDriverOrder[i];
				break;
			}
		}
		
		//console.log('[DEBUG] selected: ' + selected);
		return drivers[selected];
	} //selectDriverToUse()
	
	/**
	 * @private
	 * @return {Object} API for sessionStorage
	 */
	function makeSessionStorageDriver() {
		try {
			return global.sessionStorage;
		}
		catch (e) {
			return null;
		}
	}
	
	global['sessionforage'] = {
		'driver': driver,
		'setDriver': setDriver,
		'getItem': getItem,
		'setItem': setItem,
		'removeItem': removeItem,
		'clear': clear,
		'length': length,
		'key': key,
		'keys': keys,
		'iterate': iterate
	};
})(this);
