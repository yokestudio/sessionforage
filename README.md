# Debi #

A JavaScript library for managing client-side data persistence using an asynchronous API, inspired by [localforage](http://mozilla.github.io/localForage/).

The [Web Storage Specification](http://dev.w3.org/html5/webstorage/) is limited to storing string values, and uses a synchronous API. The popular localforage project by Mozilla enables storage of any object/serializable values, and normalizes all underlying database engines to expose an asynchronous API. This library provides a familiar API that is very similar to localforage, but with the following.

### Features ###
* Ability to store objects, numbers and anything serializeable, just like localforage.
* Built-in drivers: `indexedDB`, `localStorage`, `sessionStorage`.
* Promise-based asynchronous API

### Browser Support ###
* IE 11 (with polyfill)
* Chrome
* Edge
* Firefox
* Opera
* Safari

### Set Up ###

1. Download the latest release (as of 5 Apr 2019, v1.1.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="debi-1.1.0.min.js"></script>`
    
3. Also consider including polyfills to normalize your target environments: [ES6 Promises](https://github.com/taylorhakes/promise-polyfill)

### API ###
**getItem**

```
/**
 * Note: even if NaN / undefined is saved, null will be returned by getItem()
 * @param: {string} key
 *         {function} callback (optional)
 * @return: {Promise} resolves with value (note: value === null if not found)
 */

// Sample usage (callback)
debi.getItem('session-user', function(data) {
    console.log('Name of session-user: ' +  data.name);
});
  
// Sample usage (promises)
debi.getItem('session-user').then(function(data) {
    console.log('Name of session-user: ' +  data.name);
});
```

**setItem**
```
/**
 * @param: {string} key
 *         {string | number | anything serializable} value
 *         {function} callback (optional)
 * @return: {Promise} resolves with value
 */

// Sample usage (callback)
debi.setItem('session-user', someUser, function(data) {
    console.log('Name of session-user we just stored in sessionstorage: ' +  data.name);
});
  
// Sample usage (promises)
debi.setItem('session-user', someUser).then(function(data) {
    console.log('Name of session-user we just stored in sessionstorage: ' +  data.name);
});
```

**removeItem**
```
/**
 * @param: {string} key
 *         {function} callback (optional)
 * @return: {Promise} resolves with undefined
 */

// Sample usage (callback)
debi.removeItem('session-user', function() {
    console.log('We have removed session user from sessionstorage');
});
  
// Sample usage (promises)
debi.removeItem('session-user').then(function() {
    console.log('We have removed session user from sessionstorage');
});
```

**clear**
```
/**
 * Deletes everything from sessionStorage
 * @param: {function} callback (optional)
 * @return: {Promise} resolves with undefined
 */


// Sample usage (callback)
debi.clear(function() {
    console.log('sessionstorage is now empty');
});
  
// Sample usage (promises)
debi.clear().then(function() {
    console.log('sessionstorage is now empty');
});
```

**length**
```
/**
 * @param: {function} callback (optional)
 * @return: {Promise} resolves with number
 */

// Sample usage (callback)
debi.length(function(data) {
    console.log('sessionstorage has ' + data + ' items.');
});
  
// Sample usage (promises)
debi.length().then(function(data) {
    console.log('sessionstorage has ' + data + ' items.');
});
```

**key**
```
/**
 * Returns the name of the nth key in the list.
 * Note: the order of keys is user-agent defined. Adding/Removing items may change the order of keys,
 *       but changing the value of existing keys must not.
 *       This weird method is inherited from the WebStorage API.
 * @param: {number} n
 *         {function} callback (optional)
 * @return: {Promise} resolves with string or null
 */

// Sample usage (callback)
debi.key(4, function(data) {
    console.log('The 4th key in session storage: ' + data);
});
  
// Sample usage (promises)
debi.key(4).then(function(data) {
    console.log('The 4th key in session storage: ' + data);
});
```

**keys**
```
/**
 * Get the list of all keys in the datastore.
 * @param: {function} callback (optional)
 * @return: {Promise} resolves with array of strings
 */

// Sample usage (callback)
debi.keys(function(data) {
    console.log('The list of keys in session storage: ' + data);
});
  
// Sample usage (promises)
debi.keys().then(function(data) {
    console.log('The list of keys in session storage: ' + data);
});
```

**iterate**
```
/**
 * Iterate over all value/key pairs in datastore.
 * - Supports early exit on return (non-undefined) in the iterator function.
 * @param: {function} iterator function with three params: (value, key, iterationNumber)
 *         {function} callback (optional)
 * @return: {Promise} resolves with iterator's return value
 *                    (note: value === undefined for iterator functions without a return statement)
 */

// Let's create a simple iterator function
function logItem(v, k, i) {
    console.log('Element at position #' + i + ': ' + v + ' ==> ' + k);

    // if we encounter a key 'stop', we stop iterating.
    if (v === 'stop') {
        return null;
    }
}

// Sample usage (callback)
debi.iterate(logItem, function(data) {
    console.log('Finished logging.');
});

// Sample usage (promises)
debi.iterate(logItem).then(function(data) {
    console.log('Finished logging.');
});
```

**driver**
```
/**
 * Returns the driver in use
 */
var driver = debi.driver(); // returns "localStorage" or "sessionStorage"
```

**setDriver**
```
/**
 * Sets the preferred driver or drivers.
 * - Note: does not guarantee use of the driver; actual use depends on availability.
 * @param: {string | Array}
 */
setDriver('sessionStorage'); // forces use of sessionStorage
setDriver(['sessionStorage', 'localStorage']); // sets the preferred drivers sequence
```

### FAQ ###

1. Why don't you simply use a custom driver on localforage?
> Session persistence and local persistence are two quite different things - session data must be deleted once the browser tab is closed, local data persists indefinitely until user intervention.
>
> If we had used a custom driver on localforage, we would have conflated the two persistence concerns. It would become difficult or impossible to use the awesome localforage for both local data persistence while also using it for session storage. A separate datastore for session storage exposing a familiar API is cleaner.

1. Why can't you use both callbacks and promises together?
> Promises are meant to be an alternative approach to callback functions for asynchronous code. It simply doesn't make sense to use them both together. If you prefer the code pattern that promises provide, you'd be using Promises as much as possible; if you prefer the good old classic callbacks, you'd stay away from Promises. Using these two approaches in the same block of code sounds like a very bad, confusing practice in our books.
>
> Moreover, you'd run into unnecessary dilemma - if both a callback function and a promise-based `then()` is defined, which should the code execute first? For sanity, we make sure ONLY callbacks are executed if they are defined. In other words, we assume use of callbacks if you define both.

1. Why don't you include the promises & sessionstorage polyfills within the library?
> We believe it's a bad practice for utility (micro)libraries to include browser polyfills. If every library comes all the polyfills it needs, you would likely end up with duplicate polyfills, sometimes even incompatibility issues.
> 
> In our books, the better approach is to separate the concerns of *normalizing browser environment* and *providing utility*. Utility libraries - like store / localforage - will of course have dependencies on specific prerequisites within the environment, e.g. ES6 promises, HTML5 web storage support. If the target browsers already have support for the requisite features, including polyfills simply add redundant deadweight - worse if they're added multiple times by multiple libraries. So while it is tempting to package everything you need into a single file with no dependencies, we'd rather let users decide for themselves if they need to include polyfills, and allow users to do their own dependency management.

1. Are you related to Mozilla?
> No, but we love Mozilla and use Firefox and localforage extensively, of course. :)

### References ###
[localforage](http://mozilla.github.io/localForage/)

### Contact ###

* Email us at <yokestudio@hotmail.com>.